import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import VueI18n from 'vue-i18n';

Vue.use(Vuetify);
Vue.use(VueI18n)



// const messages = {
//     en: {
//         message: {
//             hello: 'hello world'
//         }
//     },
//     es: {
//         message: {
//             hello: 'buenos dias'
//         }
//     },
//     pt: {
//         message: {
//             hello: 'bon dia'
//         }
//     }
// }


// // Create VueI18n instance with options
// const i18n = new VueI18n({
//     locale: 'en', // set locale
//     messages, // set locale messages
// })



export default new Vuetify({

});