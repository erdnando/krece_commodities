import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
//import VueI18n from 'vue-i18n';
import i18n from '@/plugins/i18n';
import FlagIcon from 'vue-flag-icon';

Vue.config.productionTip = false

Vue.use(FlagIcon);

// const messages = {
//     en: {
//         message: {
//             hello: 'hello world'
//         }
//     },
//     es: {
//         message: {
//             hello: 'buenos dias'
//         }
//     },
//     pt: {
//         message: {
//             hello: 'bon dia'
//         }
//     }
// }


// Create VueI18n instance with options
// const i18n = new VueI18n({
//     locale: 'en', // set locale
//     messages, // set locale messages
// })




new Vue({
    router,
    store,
    vuetify,
    i18n,
    render: h => h(App)
}).$mount('#app')