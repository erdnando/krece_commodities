import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        showMenu: false,
        currentComponent: 'CARROUSEL',
        titulo1: '',
        titulo2: '',
        icono: '',
        imagen: '',
        contenido: '',
    },
    mutations: {},
    actions: {},
    modules: {}
})